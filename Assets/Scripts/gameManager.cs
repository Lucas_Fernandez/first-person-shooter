using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManager : MonoBehaviour
{
    public GameObject player;

    public GameObject bot;

    private List<GameObject> enemyArray = new List<GameObject>();

    float timeLeft;

    void Start()
    {
        StartLevel();
    }

    void Update()
    {
        if (timeLeft == 0)
        {
            StartLevel();
        }
    }

    void StartLevel()
    {
        player.transform.position = new Vector3(0f, 0f, 0f);

        foreach (GameObject item in enemyArray)
        {
            Destroy (item);
        }

        enemyArray
            .Add(Instantiate(bot, new Vector3(5, 1f, 3f), Quaternion.identity));
        enemyArray
            .Add(Instantiate(bot, new Vector3(3, 1f, 3f), Quaternion.identity));
        enemyArray
            .Add(Instantiate(bot, new Vector3(1, 1f, 3f), Quaternion.identity));
        StartCoroutine(StartCountdown(30));
    }

    public IEnumerator StartCountdown(float seconds = 30)
    {
        timeLeft = seconds;
        while (timeLeft > 0)
        {
            Debug.Log("Countdown: " + timeLeft);
            yield return new WaitForSeconds(1.0f);
            timeLeft--;
        }
    }
}
