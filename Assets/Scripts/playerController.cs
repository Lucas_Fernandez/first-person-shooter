using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{
    public float movementSpeed = 10.0f;

    public Camera firstPersonCamera;

    public GameObject projectile;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        float backAndFrontMovement = Input.GetAxis("Vertical") * movementSpeed;
        float leftAndRightMovement =
            Input.GetAxis("Horizontal") * movementSpeed;

        backAndFrontMovement *= Time.deltaTime;
        leftAndRightMovement *= Time.deltaTime;

        transform.Translate(leftAndRightMovement, 0, backAndFrontMovement);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        // if (Input.GetMouseButtonDown(0))
        // {
        //     Ray ray =
        //         firstPersonCamera
        //             .ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        //     RaycastHit hit;

        //     if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
        //     {
        //         Debug.Log("Ray has hit " + hit.collider.name);
        //     }

        //     if (hit.collider.name.Substring(0, 3) == "Bot")
        //     {
        //         GameObject hittedObject = GameObject.Find(hit.transform.name);
        //         botController scripthittedObject =
        //             (botController)
        //             hittedObject.GetComponent(typeof (botController));

        //         if (scripthittedObject != null)
        //         {
        //             scripthittedObject.takeDamage();
        //         }
        //     }
        // }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray =
                firstPersonCamera
                    .ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(projectile, ray.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb
                .AddForce(firstPersonCamera.transform.forward * 100,
                ForceMode.Impulse);

            Destroy(pro, 3.5f);
        }
    }
}
